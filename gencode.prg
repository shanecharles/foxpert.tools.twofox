*========================================================================================
* Converts all XML files back to the original files
*========================================================================================
Lparameters tcProject

	*--------------------------------------------------------------------------------------
	* Requires VFP 8 or higher, because VFP 7 has some problems with its XML support
	*--------------------------------------------------------------------------------------
	If not Version(4) >= "08.00"
		MessageBox("GenCode requires VFP 8.0 or higher")
		Return
	EndIf
	
	*--------------------------------------------------------------------------------------
	* A few settings
	*--------------------------------------------------------------------------------------
	Set Deleted on
	
	*--------------------------------------------------------------------------------------
	* Query the project file if none has been passed in
	*--------------------------------------------------------------------------------------
	Local lcProject
	If Vartype(m.tcProject) == "C" and File(m.tcProject)
		lcProject = m.tcProject
	Else
		lcProject = GetFile( "twofox", "Select a TwoFox project file" )
		If not File(m.lcProject)
			Return
		EndIf 
	EndIf
	
	*--------------------------------------------------------------------------------------
	* Create the *.pjx file.
	*--------------------------------------------------------------------------------------
	Local loMerge
	Use in Select("PJX")
	loMerge = NewObject("CMergePJX","TwoFox.prg")
	loMerge.Convert( m.lcProject )
	lcProject = ForceExt(m.lcProject,"pjx")

	*--------------------------------------------------------------------------------------
	* Go through all files and merge files
	*--------------------------------------------------------------------------------------
	Local lcFile, loConverter, loFactory
	USE (m.lcProject) Again Shared Alias PJX In 0
	loFactory = NewObject("CMergeFactory","TwoFox.prg")
	loConverter = NewObject("CConverter","TwoFox.prg","",m.loFactory)
 	loConverter.cHomeDir = Addbs(JustPath(Dbf()))
 	Select PJX
	Scan 
		lcFile = Chrtran(PJX.Name,Chr(0),"")
		DO case
		Case PJX.Type == "V"
			loConverter.Convert(m.lcFile, "vcx")
		Case PJX.Type == "K"
			loConverter.Convert(m.lcFile, "scx")
		Case PJX.Type == "R"
			loConverter.Convert(m.lcFile, "frx")
		Case PJX.Type == "B"
			loConverter.Convert(m.lcFile, "frx")
		Case PJX.Type == "M"
			loConverter.Convert(m.lcFile, "mnx")
		EndCase 
	EndScan
	USE in Select("PJX")
	Wait Clear
